package com.example.spark.controller

import com.example.spark.entity.MaritalStatus
import com.example.spark.operation.GetMaritalStatuses
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping("/maritalStatuses")
class MaritalStatusController(private val getMaritalStatuses: GetMaritalStatuses) {

    @GetMapping
    @ResponseBody
    fun getAll(): List<MaritalStatus> {

        return getMaritalStatuses.execute()
    }
}
