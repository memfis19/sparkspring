package com.example.spark.controller

import com.example.spark.entity.Gender
import com.example.spark.operation.GetGenders
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping("/genders")
class GenderController(private val getGenders: GetGenders) {

    @GetMapping
    @ResponseBody
    fun getAll(): List<Gender> {

        return getGenders.execute()
    }
}
