package com.example.spark.controller.city

import com.example.spark.entity.City
import com.example.spark.error.CityNotFound
import com.example.spark.error.NotImplemented
import com.example.spark.operation.GetCities
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/cities")
class CityController(private val getCities: GetCities) {

    @GetMapping
    @ResponseBody
    fun getAll(): List<City> {

        return getCities.execute()
    }

    @GetMapping("/{id}")
    @ResponseBody
    fun getById(@PathVariable("id") cityId: Long): City {

        throw CityNotFound()
    }

    @PutMapping("/{id}")
    @ResponseBody
    fun updateCity(@PathVariable("id") cityId: Long, city: City): City {

        throw NotImplemented
    }
}
