package com.example.spark.controller.city

import com.example.spark.dto.ApiError
import com.example.spark.error.CityNotFound
import com.example.spark.error.SparkException
import com.example.spark.error.SparkExceptionKey
import com.example.spark.error.resolveHttpStatus
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
class CityExceptionHandler(private val messageSource: MessageSource) {

    @ExceptionHandler(CityNotFound::class)
    fun handleException(exception: Exception?, request: WebRequest?): ResponseEntity<Any>? {

        val status: HttpStatus = exception?.resolveHttpStatus() ?: HttpStatus.INTERNAL_SERVER_ERROR

        val key: SparkExceptionKey = when (val cause = exception?.cause) {
            is SparkException -> cause.key
            else -> SparkExceptionKey.System.General
        }

        return ResponseEntity
            .status(status)
            .body(ApiError(status, key.resolve(messageSource)))
    }
}
