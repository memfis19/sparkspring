package com.example.spark.controller

import com.example.spark.entity.Figure
import com.example.spark.operation.GetFigures
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping("/figures")
class FigureController(private val getFigures: GetFigures) {

    @GetMapping
    @ResponseBody
    fun getAll(): List<Figure> {

        return getFigures.execute()
    }
}
