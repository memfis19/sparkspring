package com.example.spark.controller.profile

import com.example.spark.entity.Picture
import com.example.spark.entity.Profile
import com.example.spark.error.NotImplemented
import com.example.spark.error.ProfileNotFound
import com.example.spark.operation.CreateProfile
import com.example.spark.operation.GetProfileById
import com.example.spark.operation.UpdateProfile
import com.example.spark.operation.UpdateProfilePicture
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@Controller
@RequestMapping("/profiles")
class ProfileController(
    private val getProfileById: GetProfileById,
    private val createProfile: CreateProfile,
    private val updateProfile: UpdateProfile,
    private val updateProfilePicture: UpdateProfilePicture
) {

    @GetMapping
    @ResponseBody
    fun getProfiles(): List<Profile> {

        throw NotImplemented
    }

    @GetMapping("/search")
    @ResponseBody
    fun getByName(@RequestParam("name") profileName: String): List<Profile> {

        throw ProfileNotFound()
    }

    @GetMapping("/{id}")
    @ResponseBody
    fun getById(@PathVariable("id") profileId: Long): Profile {

        return getProfileById.execute(profileId)
    }

    @PostMapping
    @ResponseBody
    fun createProfile(profile: Profile): Long {

        return createProfile.execute(profile)
    }

    @PutMapping("/{id}")
    @ResponseBody
    fun updateProfile(@PathVariable("id") profileId: Long, @RequestBody profile: Profile): ResponseEntity<Unit> {

        updateProfile.execute(profileId, profile)

        return ResponseEntity.ok(Unit)
    }

    @ResponseBody
    @PostMapping("/{id}/pictures")
    fun updateProfilePhoto(
        @PathVariable("id") profileId: Long,
        @RequestPart(name = "picture") file: MultipartFile,
        @RequestPart(name = "mimeType") mimeType: String
    ): ResponseEntity<Picture> {

        return ResponseEntity.ok(updateProfilePicture.execute(profileId, file, mimeType))
    }
}
