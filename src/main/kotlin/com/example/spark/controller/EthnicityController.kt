package com.example.spark.controller

import com.example.spark.entity.Ethnicity
import com.example.spark.operation.GetEthnicities
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping("/ethnicities")
class EthnicityController(private val getEthnicities: GetEthnicities) {

    @GetMapping
    @ResponseBody
    fun getAll(): List<Ethnicity> {

        return getEthnicities.execute()
    }
}
