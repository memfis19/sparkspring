package com.example.spark.controller

import com.example.spark.entity.Religion
import com.example.spark.operation.GetReligions
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping("/religions")
class ReligionController(private val getReligions: GetReligions) {

    @GetMapping
    @ResponseBody
    fun getAll(): List<Religion> {

        return getReligions.execute()
    }
}
