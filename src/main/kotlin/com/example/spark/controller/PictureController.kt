package com.example.spark.controller

import com.example.spark.operation.GetPictureFile
import org.springframework.core.io.InputStreamResource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping("/pictures")
class PictureController(private val getPictureFile: GetPictureFile) {

    @GetMapping("/{guid}")
    @ResponseBody
    fun getPicture(@PathVariable("guid") guid: String): ResponseEntity<InputStreamResource> {

        val pictureFile = getPictureFile.execute(guid)

        return ResponseEntity.ok()
            .contentLength(pictureFile.file.length())
            .contentType(MediaType.parseMediaType(pictureFile.mimeType))
            .body(InputStreamResource(pictureFile.file.inputStream()))
    }
}
