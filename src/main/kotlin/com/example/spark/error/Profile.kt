package com.example.spark.error

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class ProfileNotFound(argument: Array<Any> = emptyArray()): SparkException(SparkExceptionKey.Profile.NotFound)

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
class ProfileNotCreated(argument: Array<Any> = emptyArray()): SparkException(SparkExceptionKey.Profile.NotCreated)

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
class ProfileNotUpdated(argument: Array<Any> = emptyArray()): SparkException(SparkExceptionKey.Profile.NotUpdated)

@ResponseStatus(value = HttpStatus.CONFLICT)
class ProfileBadAttributes(argument: Array<Any> = emptyArray()): SparkException(SparkExceptionKey.Profile.IncorrectAttributes)
