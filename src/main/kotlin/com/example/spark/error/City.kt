package com.example.spark.error

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class CityNotFound(argument: Array<Any> = emptyArray()): SparkException(SparkExceptionKey.City.NotFound)
