package com.example.spark.error

import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.util.*

sealed class SparkExceptionKey(val key: String) {

    sealed class System(key: String) : SparkExceptionKey(key) {
        object General : System("error.general")
        object NotImplemented : System("error.not_implemented")
    }

    sealed class Profile(key: String) : SparkExceptionKey(key) {
        object NotFound : Profile("error.profile.not_found")
        object NotCreated : Profile("error.profile.not_created")
        object NotUpdated : Profile("error.profile.not_updated")
        object IncorrectAttributes : Profile("error.profile.incorrect_attributes")
    }

    sealed class City(key: String) : SparkExceptionKey(key) {
        object NotFound : City("error.city.not_found")
    }

    fun resolve(
        messageSource: MessageSource,
        arguments: Array<Any> = emptyArray(),
        defaultMessage: String = "",
        locale: Locale = Locale.ROOT
    ): String {

        return messageSource.getMessage(key, arguments, defaultMessage, locale) ?: defaultMessage
    }
}

abstract class SparkException(val key: SparkExceptionKey) : Throwable()

fun Throwable.resolveHttpStatus(): HttpStatus? {

    return cause?.javaClass?.annotations
        ?.filterIsInstance(ResponseStatus::class.java)
        ?.firstOrNull()?.value
}
