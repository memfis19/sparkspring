package com.example.spark.error

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
object General : SparkException(SparkExceptionKey.System.General)

@ResponseStatus(value = HttpStatus.NOT_IMPLEMENTED)
object NotImplemented : SparkException(SparkExceptionKey.System.NotImplemented)
