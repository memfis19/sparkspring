package com.example.spark.service

import com.example.spark.dto.PictureFile
import com.example.spark.entity.Picture
import com.example.spark.entity.Profile
import com.example.spark.error.ProfileNotFound
import com.example.spark.repository.PictureRepository
import com.example.spark.repository.ProfileRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.util.*
import javax.transaction.Transactional

@Service
open class PictureService(
    @Value("\${application.picture.upload.path}") private val uploadDirectoryPath: String,
    private val profileRepository: ProfileRepository,
    private val pictureRepository: PictureRepository
) {

    object IdManager {
        fun generateGuid(): String {
            return Base64.getEncoder().encodeToString(UUID.randomUUID().toString().toByteArray(Charsets.UTF_8))
        }
    }

    @Transactional
    open fun saveFile(
        fileToUpload: MultipartFile,
        mimeType: String,
        createBy: Profile
    ): Picture {

        val guid = IdManager.generateGuid()

        val folderPath = getPath()
        val filePath = folderPath + "/" + guid + "." + fileToUpload.originalFilename?.substringAfterLast('.', "")

        val folder = File(folderPath)
        if (!folder.exists()) folder.mkdirs()

        val fileToSave = File(filePath)
        fileToSave.mkdirs()
        fileToSave.mkdir()
        fileToSave.createNewFile()

        fileToUpload.transferTo(fileToSave)

        return pictureRepository.save(Picture().apply {
            this.guid = guid
            this.mimeType = mimeType
            this.path = filePath//todo: consider to use relative path in case of data migration
            this.createdBy = createBy.id
            this.size = fileToSave.length()
        })
    }

    open fun getPictureFile(guid: String): PictureFile {

        val picture = pictureRepository.findById(guid).orElseThrow { ProfileNotFound() }
        val pictureFile = File(picture.path)

        return if (pictureFile.exists()) {
            PictureFile(pictureFile, picture.mimeType)
        } else {
            TODO("Picture Not Found")
        }
    }

    private fun getPath(): String {

        return File(".").canonicalPath + uploadDirectoryPath
    }
}
