package com.example.spark.service

import com.example.spark.entity.Picture
import com.example.spark.entity.Profile
import com.example.spark.error.ProfileBadAttributes
import com.example.spark.error.ProfileNotFound
import com.example.spark.error.ProfileNotUpdated
import com.example.spark.repository.ProfileRepository
import org.springframework.stereotype.Service

@Service
class ProfileService(
    private val profileRepository: ProfileRepository,
    private val attributesAvailabilityService: AttributesAvailabilityService
) {

    fun updateProfile(profileId: Long, profile: Profile) {

        val profileToUpdate = profileRepository.findById(profileId).orElseThrow { ProfileNotFound() }
        val isValid = profile.id == profileToUpdate.id
                && profile.displayName.isNotEmpty()
                && profile.realName.isNotEmpty()
                && attributesAvailabilityService.isCityAvailable(profile.location, true)
                && attributesAvailabilityService.isGenderAvailable(profile.gender, true)
                && attributesAvailabilityService.isMaritalStatusAvailable(profile.maritalStatus, true)
                && attributesAvailabilityService.isEthnicityAvailable(profile.ethnicity)
                && attributesAvailabilityService.isFigureAvailable(profile.figure)
                && attributesAvailabilityService.isReligionAvailable(profile.religion)

        if (isValid) {

            profileRepository.save(profile).id ?: throw ProfileNotUpdated()
        } else {

            throw ProfileBadAttributes()
        }
    }

    fun updatePicture(profileId: Long, picture: Picture) {

        val profileToUpdate = profileRepository.findById(profileId).orElseThrow { ProfileNotFound() }
        profileToUpdate.picture = picture
        profileRepository.save(profileToUpdate)
    }
}
