package com.example.spark.service

import com.example.spark.entity.*
import com.example.spark.repository.*
import org.springframework.stereotype.Service

interface AttributesAvailabilityService {
    fun isEthnicityAvailable(ethnicity: Ethnicity?, required: Boolean = false): Boolean
    fun isCityAvailable(city: City?, required: Boolean = false): Boolean
    fun isGenderAvailable(gender: Gender?, required: Boolean = false): Boolean
    fun isMaritalStatusAvailable(maritalStatus: MaritalStatus?, required: Boolean = false): Boolean
    fun isReligionAvailable(religion: Religion?, required: Boolean = false): Boolean
    fun isFigureAvailable(figure: Figure?, required: Boolean = false): Boolean
}

@Service
class AttributesAvailabilityServiceImpl(
        private val cityRepository: CityRepository,
        private val genderRepository: GenderRepository,
        private val maritalStatusRepository: MaritalStatusRepository,
        private val religionRepository: ReligionRepository,
        private val ethnicityRepository: EthnicityRepository,
        private val figureRepository: FigureRepository
) : AttributesAvailabilityService {

    override fun isEthnicityAvailable(ethnicity: Ethnicity?, required: Boolean): Boolean {

        val nonNull = ethnicity ?: return !required
        val ethnicityId = nonNull.id ?: return false

        return ethnicityRepository.findById(ethnicityId).isPresent
    }

    override fun isCityAvailable(city: City?, required: Boolean): Boolean {

        val nonNull = city ?: return !required
        val cityId = nonNull.id ?: return false

        return cityRepository.findById(cityId).isPresent
    }

    override fun isGenderAvailable(gender: Gender?, required: Boolean): Boolean {

        val nonNull = gender ?: return !required
        val genderId = nonNull.id ?: return false

        return genderRepository.findById(genderId).isPresent
    }

    override fun isMaritalStatusAvailable(maritalStatus: MaritalStatus?, required: Boolean): Boolean {

        val nonNull = maritalStatus ?: return !required
        val maritalStatusId = nonNull.id ?: return false

        return maritalStatusRepository.findById(maritalStatusId).isPresent
    }

    override fun isReligionAvailable(religion: Religion?, required: Boolean): Boolean {

        val nonNull = religion ?: return !required
        val religionId = nonNull.id ?: return false

        return religionRepository.findById(religionId).isPresent
    }

    override fun isFigureAvailable(figure: Figure?, required: Boolean): Boolean {

        val nonNull = figure ?: return !required
        val figureId = nonNull.id ?: return false

        return figureRepository.findById(figureId).isPresent
    }
}
