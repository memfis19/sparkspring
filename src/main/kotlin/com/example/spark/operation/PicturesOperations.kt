package com.example.spark.operation

import com.example.spark.dto.PictureFile
import com.example.spark.service.PictureService
import org.springframework.stereotype.Component

@Component
class GetPictureFile(
    private val pictureService: PictureService
) {

    fun execute(guid: String): PictureFile {

        return pictureService.getPictureFile(guid)
    }
}
