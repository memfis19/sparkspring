package com.example.spark.operation

import com.example.spark.entity.Picture
import com.example.spark.entity.Profile
import com.example.spark.error.ProfileNotCreated
import com.example.spark.error.ProfileNotFound
import com.example.spark.repository.ProfileRepository
import com.example.spark.service.PictureService
import com.example.spark.service.ProfileService
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile

@Component
class SeedProfile(private val profileRepository: ProfileRepository) {

    fun execute(profile: Profile) {

        if (profileRepository.count() == 0L) {

            profileRepository.save(profile)
        }
    }
}

@Component
class GetProfileById(private val profileRepository: ProfileRepository) {

    fun execute(id: Long): Profile {

        return profileRepository.findById(id).orElseThrow {

            ProfileNotFound()
        }
    }
}

@Component
class CreateProfile(private val profileRepository: ProfileRepository) {

    fun execute(profile: Profile): Long {

        return profileRepository.save(profile).id ?: throw ProfileNotCreated()
    }
}

@Component
class UpdateProfile(private val profileService: ProfileService) {

    fun execute(profileId: Long, profile: Profile) {

        profileService.updateProfile(profileId, profile)
    }
}

@Component
class UpdateProfilePicture(
    private val profileRepository: ProfileRepository,
    private val profileService: ProfileService,
    private val pictureService: PictureService
) {

    fun execute(profileId: Long, file: MultipartFile, mimeType: String): Picture {

        val profile = profileRepository.findById(profileId).orElseThrow { ProfileNotFound() }
        val picture = pictureService.saveFile(file, mimeType, profile)
        profileService.updatePicture(profileId, picture)

        return picture
    }
}
