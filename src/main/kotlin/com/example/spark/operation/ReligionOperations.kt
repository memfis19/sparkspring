package com.example.spark.operation

import com.example.spark.entity.Religion
import com.example.spark.repository.ReligionRepository
import org.springframework.stereotype.Component

@Component
class GetReligions(private val religionRepository: ReligionRepository) {

    fun execute(): List<Religion> {

        return religionRepository.findAll()
    }
}

@Component
class SeedReligions(private val religionRepository: ReligionRepository) {

    fun execute(religions: List<Religion>): List<Religion> {

        return if (religionRepository.count() == 0L) {

            religionRepository.saveAll(religions)
        } else {

            religionRepository.findAll()
        }
    }
}
