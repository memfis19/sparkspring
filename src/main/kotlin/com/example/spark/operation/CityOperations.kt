package com.example.spark.operation

import com.example.spark.entity.City
import com.example.spark.repository.CityRepository
import org.springframework.stereotype.Component

@Component
class GetCities(private val cityRepository: CityRepository) {

    fun execute(): List<City> {

        return cityRepository.findAll()
    }
}

@Component
class SeedCities(private val cityRepository: CityRepository) {

    fun execute(cities: List<City>): List<City> {

        return if (cityRepository.count() == 0L) {

            cityRepository.saveAll(cities)
        } else {

            cityRepository.findAll()
        }
    }
}
