package com.example.spark.operation

import com.example.spark.entity.MaritalStatus
import com.example.spark.repository.MaritalStatusRepository
import org.springframework.stereotype.Component

@Component
class GetMaritalStatuses(private val maritalStatusRepository: MaritalStatusRepository) {

    fun execute(): List<MaritalStatus> {

        return maritalStatusRepository.findAll()
    }
}

@Component
class SeedMaritalStatuses(private val maritalStatusRepository: MaritalStatusRepository) {

    fun execute(maritalStatuses: List<MaritalStatus>): List<MaritalStatus> {

        return if (maritalStatusRepository.count() == 0L) {

            maritalStatusRepository.saveAll(maritalStatuses)
        } else {

            maritalStatusRepository.findAll()
        }
    }
}
