package com.example.spark.operation

import com.example.spark.entity.Gender
import com.example.spark.repository.GenderRepository
import org.springframework.stereotype.Component

@Component
class GetGenders(private val genderRepository: GenderRepository) {

    fun execute(): List<Gender> {

        return genderRepository.findAll()
    }
}

@Component
class SeedGenders(private val genderRepository: GenderRepository) {

    fun execute(genders: List<Gender>): List<Gender> {

        return if (genderRepository.count() == 0L) {

            genderRepository.saveAll(genders)
        } else {

            genderRepository.findAll()
        }
    }
}
