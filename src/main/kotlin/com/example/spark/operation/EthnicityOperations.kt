package com.example.spark.operation

import com.example.spark.entity.Ethnicity
import com.example.spark.repository.EthnicityRepository
import org.springframework.stereotype.Component

@Component
class GetEthnicities(private val ethnicityRepository: EthnicityRepository) {

    fun execute(): List<Ethnicity> {

        return ethnicityRepository.findAll()
    }
}

@Component
class SeedEthnicities(private val ethnicityRepository: EthnicityRepository) {

    fun execute(ethnicities: List<Ethnicity>): List<Ethnicity> {

        return if (ethnicityRepository.count() == 0L) {

            ethnicityRepository.saveAll(ethnicities)
        } else {

            ethnicityRepository.findAll()
        }
    }
}
