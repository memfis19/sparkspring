package com.example.spark.operation

import com.example.spark.entity.Figure
import com.example.spark.repository.FigureRepository
import org.springframework.stereotype.Component

@Component
class GetFigures(private val figureRepository: FigureRepository) {

    fun execute(): List<Figure> {

        return figureRepository.findAll()
    }
}

@Component
class SaveFigures(private val figureRepository: FigureRepository) {

    fun execute(figures: List<Figure>): List<Figure> {

        return if (figureRepository.count() == 0L) {

            figureRepository.saveAll(figures)
        } else {

            figureRepository.findAll()
        }
    }
}
