package com.example.spark.repository

import com.example.spark.entity.Figure
import org.springframework.data.jpa.repository.JpaRepository

interface FigureRepository : JpaRepository<Figure, String>
