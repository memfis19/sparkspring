package com.example.spark.repository

import com.example.spark.entity.Ethnicity
import org.springframework.data.jpa.repository.JpaRepository

interface EthnicityRepository : JpaRepository<Ethnicity, String>
