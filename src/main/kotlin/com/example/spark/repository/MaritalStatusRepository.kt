package com.example.spark.repository

import com.example.spark.entity.MaritalStatus
import org.springframework.data.jpa.repository.JpaRepository

interface MaritalStatusRepository : JpaRepository<MaritalStatus, String>
