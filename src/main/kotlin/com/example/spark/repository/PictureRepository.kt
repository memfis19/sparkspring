package com.example.spark.repository

import com.example.spark.entity.Picture
import org.springframework.data.jpa.repository.JpaRepository

interface PictureRepository : JpaRepository<Picture, String>
