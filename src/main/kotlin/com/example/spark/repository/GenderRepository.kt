package com.example.spark.repository

import com.example.spark.entity.Gender
import org.springframework.data.jpa.repository.JpaRepository

interface GenderRepository : JpaRepository<Gender, String>
