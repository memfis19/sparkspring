package com.example.spark.repository

import com.example.spark.entity.Religion
import org.springframework.data.jpa.repository.JpaRepository

interface ReligionRepository : JpaRepository<Religion, String>
