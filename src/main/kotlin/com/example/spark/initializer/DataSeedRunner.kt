package com.example.spark.initializer

import com.example.spark.entity.*
import com.example.spark.operation.*
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import java.io.File
import java.util.*
import kotlin.math.sin

@Component
class DataSeedRunner(private val objectMapper: ObjectMapper,
                     private val seedCities: SeedCities,
                     private val seedEthnicities: SeedEthnicities,
                     private val seedFigures: SaveFigures,
                     private val seedGenders: SeedGenders,
                     private val seedMaritalStatuses: SeedMaritalStatuses,
                     private val seedReligions: SeedReligions,
                     private val seedProfile: SeedProfile) : ApplicationRunner {

    data class Cities(val cities: List<City>)

    private val cities: File by lazy {

        File("seed/cities.json")
    }

    data class SingleChoiceAttributes(
            val gender: List<Gender>,
            val ethnicity: List<Ethnicity>,
            val religion: List<Religion>,
            val figure: List<Figure>,
            @JsonProperty("marital_status") val maritaStatus: List<MaritalStatus>
    )

    private val singleChoiceAttributes: File by lazy {

        File("seed/single_choice_attributes.json")
    }

    override fun run(args: ApplicationArguments?) {

        val citiesJson = cities.readText()

        val cities = objectMapper.readValue(citiesJson, Cities::class.java)
        val savedCities = seedCities.execute(cities.cities)

        val scaJson = singleChoiceAttributes.readText()
        val singleChoiceAttributes = objectMapper.readValue(scaJson, SingleChoiceAttributes::class.java)

        val savedEthnicities = seedEthnicities.execute(singleChoiceAttributes.ethnicity)
        val savedFigures= seedFigures.execute(singleChoiceAttributes.figure)
        val savedGenders = seedGenders.execute(singleChoiceAttributes.gender)
        val savedMS = seedMaritalStatuses.execute(singleChoiceAttributes.maritaStatus)
        val savedReligions = seedReligions.execute(singleChoiceAttributes.religion)

        val profile = Profile().apply {
            displayName = "Test"
            realName = "Test"
            birthdate = Calendar.getInstance().let { calendar ->
                calendar.set(1990, 1, 1)
                calendar.time
            }
            height = 185.5
            gender = savedGenders.randomOrNull() ?: return
            maritalStatus = savedMS.randomOrNull() ?: return
            religion = savedReligions.randomOrNull() ?: return
            figure = savedFigures.randomOrNull() ?: return
            ethnicity = savedEthnicities.randomOrNull() ?: return
            occupation = "Occupation of Test"
            aboutMe = "AboutMe of Test"
            location = savedCities.randomOrNull() ?: return
        }
        seedProfile.execute(profile)
    }
}
