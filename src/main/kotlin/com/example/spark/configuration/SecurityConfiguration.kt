package com.example.spark.configuration

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class SecurityConfiguration : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.csrf().disable()
                .cors()
                .and().authorizeRequests()
                .antMatchers("/cities/**").permitAll()
                .antMatchers("/genders").permitAll()
                .antMatchers("/figures").permitAll()
                .antMatchers("/ethnicities").permitAll()
                .antMatchers("/maritalStatuses").permitAll()
                .antMatchers("/religions").permitAll()
                .antMatchers("/profiles/**").permitAll()
                .antMatchers("/pictures/**").permitAll()
                .anyRequest().authenticated()
    }
}
