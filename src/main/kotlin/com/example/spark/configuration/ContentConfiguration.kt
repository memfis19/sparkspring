package com.example.spark.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import java.text.SimpleDateFormat
import java.util.*

@Configuration
class ContentConfiguration {

    @Bean
    @Primary
    fun objectMapper(): ObjectMapper {

        return ObjectMapper()
            .setDateFormat(SimpleDateFormat(RFC822MS, Locale.ROOT))
            .registerKotlinModule()
    }

    companion object {
        const val ISO8601MS: String = "yyyy-MM-dd'T'HH:mm:ss.SSSX"
        const val RFC822MS: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    }
}
