package com.example.spark.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = Religion.TABLE_NAME)
class Religion {

    @Id
    var id: String? = null

    @Column(name = NAME)
    var name: String = ""

    companion object {

        const val TABLE_NAME: String = "religion"
        const val NAME: String = "name"
    }
}
