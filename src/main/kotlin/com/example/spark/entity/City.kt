package com.example.spark.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*

@Entity
@Table(name = City.TABLE_NAME)
@JsonIgnoreProperties(ignoreUnknown = true)
class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @JsonProperty(CITY)
    @Column(name = CITY)
    var city: String = ""

    @JsonProperty(LATITUDE)
    @Column(name = LATITUDE)
    var latitude: String = ""

    @JsonProperty(LONGITUDE)
    @Column(name = LONGITUDE)
    var longitude: String = ""

    companion object {

        const val TABLE_NAME: String = "city"
        const val CITY: String = "city"
        const val LATITUDE: String = "lat"
        const val LONGITUDE: String = "lon"
    }
}
