package com.example.spark.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = MaritalStatus.TABLE_NAME)
class MaritalStatus {

    @Id
    var id: String? = null

    @Column(name = NAME)
    var name: String = ""

    companion object {

        const val TABLE_NAME: String = "marital_status"
        const val NAME: String = "name"
    }
}
