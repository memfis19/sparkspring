package com.example.spark.entity

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*
import javax.persistence.*

@Entity
@Table(name = Profile.TABLE_NAME)
@JsonIgnoreProperties(ignoreUnknown = true)
class Profile() {

    @JsonCreator
    constructor(
            id: Long?,
            displayName: String,
            realName: String,
            birthdate: Date,
            height: Double,
            picture: Picture?,
            gender: Gender?,
            ethnicity: Ethnicity?,
            religion: Religion?,
            maritalStatus: MaritalStatus?,
            figure: Figure?,
            occupation: String,
            aboutMe: String,
            location: City?
    ) : this() {

        this.id = id
        this.displayName = displayName
        this.realName = realName
        this.birthdate = birthdate
        this.height = height
        this.picture = picture
        this.gender = gender
        this.ethnicity = ethnicity
        this.religion = religion
        this.maritalStatus = maritalStatus
        this.figure = figure
        this.occupation = occupation
        this.aboutMe = aboutMe
        this.location = location
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    var displayName: String = ""

    var realName: String = ""

    var birthdate: Date = Date()

    var height: Double = 0.0

    @ManyToOne
    @JoinColumn(name = PICTURE)
    var picture: Picture? = null

    @ManyToOne
    @JoinColumn(name = GENDER)
    var gender: Gender? = null

    @ManyToOne
    @JoinColumn(name = ETHNICITY)
    var ethnicity: Ethnicity? = null

    @ManyToOne
    @JoinColumn(name = RELIGION)
    var religion: Religion? = null

    @ManyToOne
    @JoinColumn(name = MARITAL_STATUS)
    var maritalStatus: MaritalStatus? = null

    @ManyToOne
    @JoinColumn(name = FIGURE)
    var figure: Figure? = null

    var occupation: String = ""

    var aboutMe: String = ""

    @ManyToOne
    @JoinColumn(name = CITY)
    var location: City? = null

    companion object {

        const val TABLE_NAME: String = "profile"
        const val PICTURE: String = "picture"
        const val GENDER: String = "gender"
        const val ETHNICITY: String = "ethnicity"
        const val RELIGION: String = "religion"
        const val FIGURE: String = "figure"
        const val MARITAL_STATUS: String = "marital_status"
        const val CITY: String = "city"
    }
}
