package com.example.spark.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.LazyToOne
import org.hibernate.annotations.LazyToOneOption
import org.springframework.context.annotation.Lazy
import java.util.*
import javax.persistence.*

@Entity
@Table(name = Picture.TABLE_NAME)
class Picture() {

    constructor(guid: String, createdAt: Date, mimeType: String, size: Long?) : this() {
        this.guid = guid
        this.createdAt = createdAt
        this.mimeType = mimeType
        this.size = size
    }

    @Id
    @Column(unique = true)
    var guid: String? = null

    @Column(name = CREATED_AT)
    var createdAt: Date = Date()

    @JsonIgnore
    @Column(name = CREATED_BY)
    var createdBy: Long? = null

    @Column(name = MIME_TYPE)
    var mimeType: String = ""

    @JsonIgnore
    var path: String = ""

    @JsonIgnore
    var size: Long? = null

    companion object {
        const val TABLE_NAME: String = "picture"
        const val CREATED_AT: String = "created_at"
        const val CREATED_BY: String = "created_by"
        const val MIME_TYPE: String = "mime_type"
    }
}
