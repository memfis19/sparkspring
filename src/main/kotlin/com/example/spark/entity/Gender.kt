package com.example.spark.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = Gender.TABLE_NAME)
class Gender {

    @Id
    var id: String? = null

    @Column(name = NAME)
    var name: String = ""

    companion object {

        const val TABLE_NAME: String = "gender"
        const val NAME: String = "name"
    }
}
