package com.example.spark.dto

import org.springframework.http.HttpStatus
import java.io.File

data class ApiError(val status: HttpStatus, val message: String)

data class PictureFile(val file: File, val mimeType: String)
