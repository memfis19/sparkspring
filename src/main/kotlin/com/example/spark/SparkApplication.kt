package com.example.spark

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SparkApplication

fun main(args: Array<String>) {
    runApplication<SparkApplication>(*args)
}
