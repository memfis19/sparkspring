package com.example.spark.controller

import com.example.spark.entity.Profile
import com.example.spark.repository.ProfileRepository
import com.example.spark.service.AttributesAvailabilityService
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers.containsString
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*
import kotlin.random.Random

@AutoConfigureJsonTesters
@SpringBootTest
@AutoConfigureMockMvc
class ProfileControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var mockAAS: AttributesAvailabilityService

    @MockBean
    private lateinit var mockProfileRepository: ProfileRepository

    @Test
    fun `should return profile when exists`() {

        val existingProfileId: Long = 1
        val existingProfile = Profile().apply {
            id = existingProfileId
            displayName = "Test"
            aboutMe = "About me"
        }
        val profileJson = objectMapper.writeValueAsString(existingProfile)

        Mockito.`when`(mockProfileRepository.findById(1))
                .thenReturn(Optional.of(existingProfile))

        mockMvc.perform(get("http://localhost/profiles/$existingProfileId"))
                .andExpect(status().isOk)
                .andExpect(content().string(containsString(profileJson)))
    }

    @Test
    fun `should return not found(404) when profile not found`() {

        val existingProfileId: Long = 1

        Mockito.`when`(mockProfileRepository.findById(any()))
                .thenReturn(Optional.empty())

        mockMvc.perform(get("http://localhost/profiles/$existingProfileId"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `should return profile id when profile created successfully`() {

        val newProfile = Profile().apply {
            displayName = "Test"
            aboutMe = "About me"
        }
        val jsonProfile = objectMapper.writeValueAsString(newProfile)

        val newProfileId = Random.nextLong(1L, 1000L)
        Mockito.`when`(mockProfileRepository.save(any()))
                .thenReturn(newProfile.apply {
                    id = newProfileId
                })

        mockMvc.perform(post("http://localhost/profiles").content(jsonProfile))
                .andExpect(status().isOk)
                .andExpect(content().string(containsString(newProfileId.toString())))
    }

    @Test
    fun `should return bad request when profile hasn't been created`() {

        val newProfile = Profile().apply {
            displayName = "Test"
            aboutMe = "About me"
        }
        val jsonProfile = objectMapper.writeValueAsString(newProfile)

        Mockito.`when`(mockProfileRepository.save(any())).thenReturn(newProfile)

        mockMvc.perform(post("http://localhost/profiles").content(jsonProfile))
                .andExpect(status().isBadRequest)
    }

    @Test
    fun `should return status OK(200) when profile updated successfully`() {

        val profileId = Random.nextLong(1L, 1000L)
        val profile = Profile().apply {
            id = profileId
            displayName = "Test"
            realName = "Test"
        }
        val jsonProfile = objectMapper.writeValueAsString(profile)

        Mockito.`when`(mockAAS.isCityAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isEthnicityAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isGenderAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isReligionAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isMaritalStatusAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isFigureAvailable(any(), anyBoolean())).thenReturn(true)

        Mockito.`when`(mockProfileRepository.findById(profileId)).thenReturn(Optional.of(profile))
        Mockito.`when`(mockProfileRepository.save(any())).thenReturn(profile)

        mockMvc.perform(
                put("http://localhost/profiles/$profileId")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonProfile))
                .andExpect(status().isOk)
    }

    @Test
    fun `should return status NotFound(404) when profile id does not exist`() {

        val profileId = Random.nextLong(1L, 1000L)
        val otherProfileId = Random.nextLong(1L, 1000L)
        val profile = Profile().apply {
            id = profileId
            displayName = "Test"
            realName = "Test"
        }
        val jsonProfile = objectMapper.writeValueAsString(profile)

        Mockito.`when`(mockAAS.isCityAvailable(any(), anyBoolean())).thenReturn(false)
        Mockito.`when`(mockAAS.isEthnicityAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isGenderAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isReligionAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isMaritalStatusAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isFigureAvailable(any(), anyBoolean())).thenReturn(true)

        Mockito.`when`(mockProfileRepository.findById(otherProfileId)).thenReturn(Optional.empty())
        Mockito.`when`(mockProfileRepository.findById(profileId)).thenReturn(Optional.of(profile))
        Mockito.`when`(mockProfileRepository.save(any())).thenReturn(profile)

        mockMvc.perform(
                put("http://localhost/profiles/$otherProfileId")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonProfile))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `should return status NotValid(409) when profile id does not match`() {

        val profileId = Random.nextLong(1L, 1000L)
        val profile = Profile().apply {
            id = profileId
            displayName = "Test"
            realName = "Test"
        }
        val otherProfileId = Random.nextLong(1L, 1000L)
        val otherProfile = Profile().apply {
            id = otherProfileId
            displayName = "Test"
            realName = "Test"
        }

        val jsonProfile = objectMapper.writeValueAsString(profile)

        Mockito.`when`(mockAAS.isCityAvailable(any(), anyBoolean())).thenReturn(false)
        Mockito.`when`(mockAAS.isEthnicityAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isGenderAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isReligionAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isMaritalStatusAvailable(any(), anyBoolean())).thenReturn(true)
        Mockito.`when`(mockAAS.isFigureAvailable(any(), anyBoolean())).thenReturn(true)

        Mockito.`when`(mockProfileRepository.findById(profileId)).thenReturn(Optional.of(profile))
        Mockito.`when`(mockProfileRepository.findById(otherProfileId)).thenReturn(Optional.of(otherProfile))
        Mockito.`when`(mockProfileRepository.save(any())).thenReturn(profile)

        mockMvc.perform(
                put("http://localhost/profiles/$otherProfileId")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonProfile))
                .andExpect(status().isConflict)
    }
}
