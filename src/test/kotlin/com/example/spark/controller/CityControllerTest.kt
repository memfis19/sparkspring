package com.example.spark.controller

import com.example.spark.entity.City
import com.example.spark.repository.CityRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.util.*
import kotlin.random.Random

@AutoConfigureJsonTesters
@SpringBootTest
@AutoConfigureMockMvc
class CityControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var mockCityRepository: CityRepository

    @Test
    fun `should return empty list if no cities`() {

        val emptyCitiesJson = objectMapper.writeValueAsString(listOf<City>())

        Mockito.`when`(mockCityRepository.findAll()).thenReturn(emptyList())

        mockMvc.perform(get("http://localhost/cities"))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(emptyCitiesJson)))
    }

    @Test
    fun `should return list of cities which match DB list`() {

        val city = City().apply {
            id = Random.nextLong(1, 1000)
            city = UUID.randomUUID().toString()
            latitude = Random.nextDouble(1.0, 1000.0).toString()
            longitude = Random.nextDouble(1.0, 1000.0).toString()
        }

        val citiesJson = objectMapper.writeValueAsString(listOf(city))
        Mockito.`when`(mockCityRepository.findAll()).thenReturn(listOf(city))

        mockMvc.perform(get("http://localhost/cities"))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(citiesJson)))
    }
}
