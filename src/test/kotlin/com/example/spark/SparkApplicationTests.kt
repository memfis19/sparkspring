package com.example.spark

import com.example.spark.repository.ProfileRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.util.Assert

@SpringBootTest
class SparkApplicationTests {

    @Autowired
    private var profileRepository: ProfileRepository? = null

    @Test
    fun contextLoads() {

       Assertions.assertNotNull(profileRepository)
    }
}
